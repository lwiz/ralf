Copyright (c) 2022 Louise Montalvo louanmontalvo@gmail.com

# The RALF Programming Language

This is a simple assembly-like language that is in early development.
Eventually I hope to write a VM or compiler for it but for now it is 
interpreted. Currently it is faster than Python in computing fib(12).

## Usage

To see examples of possible code constructs look at the scripts in `test/`.
You can run scripts by providing the name as an argument as follows:
`cargo run -- scriptname`. For example: `cargo run -- ./test/fib.ralf`
