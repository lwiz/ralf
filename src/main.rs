#![deny(rust_2018_idioms)]

#![warn(clippy::pedantic)]
#![allow(clippy::or_fun_call)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::too_many_lines)]
#![allow(clippy::similar_names)]
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::cast_precision_loss)]

use std::{fs, io};
use std::io::Write;
use std::collections::HashMap;

#[derive(Debug, Clone)]
struct Line {
    pos: String,
    args: Vec<String>,
}
#[derive(Debug, Clone)]
struct Function {
    mempos: usize,
    args: Vec<String>,
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum Instr {
    Alias = 0,
    Add,
    Call,
    Div,
    Input,
    Jmp,
    JmpEq,
    JmpLeq,
    Label,
    Mod,
    Mul,
    Pop,
    Print,
    Println,
    PrintMem,
    Push,
    Ret,
    Set,
    Sqrt,
    Sub,
}
impl TryFrom<u64> for Instr {
    type Error = String;

    fn try_from(instr: u64) -> Result<Self, Self::Error> {
        match instr {
            0 => Ok(Instr::Alias),
            1 => Ok(Instr::Add),
            2 => Ok(Instr::Call),
            3 => Ok(Instr::Div),
            4 => Ok(Instr::Input),
            5 => Ok(Instr::Jmp),
            6 => Ok(Instr::JmpEq),
            7 => Ok(Instr::JmpLeq),
            8 => Ok(Instr::Label),
            9 => Ok(Instr::Mod),
            10 => Ok(Instr::Mul),
            11 => Ok(Instr::Pop),
            12 => Ok(Instr::Print),
            13 => Ok(Instr::Println),
            14 => Ok(Instr::PrintMem),
            15 => Ok(Instr::Push),
            16 => Ok(Instr::Ret),
            17 => Ok(Instr::Set),
            18 => Ok(Instr::Sqrt),
            19 => Ok(Instr::Sub),
            _ => Err(format!("invalid Instr code {instr}")),
        }
    }
}

#[derive(Debug, Clone)]
enum Alias {
    Reg(u64),
    Stack(u64),
}
impl Alias {
    fn resolve(&self, memory: &Memory) -> u64 {
        match self {
            Alias::Reg(loc) => memory.reg[*loc as usize],
            Alias::Stack(loc) => memory.stack[*loc as usize],
        }
    }
    fn resolve_array<'a>(&self, memory: &'a Memory) -> Result<&'a [u64], String> {
        match self {
            Alias::Stack(loc) => {
                let size = memory.stack[*loc as usize] as usize;
                Ok(&memory.stack[*loc as usize+1..=*loc as usize+size])
            },
            Alias::Reg(_) => Err("can't resolve array on registers".into()),
        }
    }
    fn get_and_resolve(memory: &Memory, al: &str) -> Result<u64, String> {
        match Alias::get(memory, al) {
            Ok(a) => Ok(a.resolve(memory)),
            Err(m) => match al.parse::<u64>() {
                Ok(v) => Ok(v),
                Err(_) => Err(m),
            },
        }
    }
    fn get(memory: &Memory, al: &str) -> Result<Alias, String> {
        if al.starts_with('*') {
            let r = Alias::get(memory, al.strip_prefix('*')
                .ok_or("dereference missing '*' prefix".to_owned())?)?;
            Ok(Alias::Stack(r.resolve(memory)))
        } else if al.starts_with('R') {
            let r = al.strip_prefix('R')
                .ok_or("register missing 'R' prefix".to_owned())?
                .parse::<u64>()
                    .map_err(|e| format!("{e}"))?;
            Ok(Alias::Reg(r - 1))
        } else if al.starts_with('S') {
            let s = al.strip_prefix('S')
                .ok_or("stack address missing 'S' prefix".to_owned())?
                .parse::<u64>()
                    .map_err(|e| format!("{e}"))?;
            Ok(Alias::Stack(memory.stack.len() as u64 - s - 1))
        } else {
            match memory.aliases.last() {
                Some(aliases) => aliases.get(al).cloned().ok_or(format!("invalid alias {al}")),
                None => Err(format!("missing aliases table when looking up {al}")),
            }
        }
    }
}

#[derive(Debug, Clone)]
struct Memory {
    reg: [u64; 16],
    stack: Vec<u64>,
    code: Vec<u64>,

    aliases: Vec<HashMap<String, Alias>>,
    funcs: HashMap<String, Function>,
    labels: HashMap<String, Vec<usize>>,

    debug: HashMap<usize, String>,
}
impl Memory {
    fn blank() -> Memory {
        Memory {
            reg: [0; 16],
            stack: vec![],
            code: vec![],
            
            aliases: vec![HashMap::new()],
            funcs: HashMap::new(),
            labels: HashMap::new(),
            
            debug: HashMap::new(),
        }
    }
    fn push_argv(&mut self) -> [u64; 1] {
        let argv = std::env::args().skip(1);

        let mut argvec: Vec<u64> = vec![];
        for arg in argv {
            argvec.push(self.stack.len() as u64);
            self.stack.push(arg.chars().count() as u64);
            for c in arg.chars() {
                self.stack.push(c as u64);
            }
        }
        let av = self.stack.len();
        self.stack.push(argvec.len() as u64);
        for a in argvec {
            self.stack.push(a);
        }

        [av as u64]
    }
}

fn split_whitespace_quote(s: &str) -> Vec<String> {
    let sw: Vec<&str> = s.split(' ').collect();
    let mut swq: Vec<String> = vec![];
    let mut i = 0;
    while i < sw.len() {
        if !sw[i].is_empty() { // Skip empties
            let mut ns = sw[i].to_string();
            
            // Combine quoted strings
            if sw[i].starts_with('"') && sw[i].split('"').count() % 2 == 0 {
                i += 1;

                while i < sw.len() {
                    let rs = sw[i];
                    ns = format!("{ns} {rs}");

                    if sw[i].ends_with('"') {
                        break;
                    }

                    i += 1;
                }
            }

            // Append split string
            swq.push(ns);
        }

        i += 1;
    }
    swq
}
fn split(filename: &str, code: &str) -> Vec<Line> {
    code.lines()
        .enumerate()
        .filter(|(_, l)| !l.trim().is_empty()) // Skip empties
        .filter(|(_, l)| !l.trim().starts_with('#')) // Skip comments
        .map(|(i, l)| (i+1, l))
        .map(|(i, l)| Line {
            pos: format!("{filename}:{i}"),
            args: split_whitespace_quote(l),
        }).collect()
}

fn push_str(region: &mut Vec<u64>, s: &str) {
    region.push(s.len() as u64);
    for b in s.as_bytes() {
        region.push(u64::from(*b));
    }
}
fn pull_str(region: &[u64], pos: usize) -> String {
    let l = region[pos];
    region.iter()
        .skip(pos+1)
        .take(l as usize)
        .map(|c| *c as u8 as char)
        .collect()
}
fn next_label(lpos: &[usize], pos: usize) -> usize {
    let mut lpos = lpos.to_vec();
    lpos.sort_unstable();

    for &p in &lpos {
        if p > pos {
            return p;
        }
    }

    lpos[0]
}
fn compile_bytecode(lines: &[Line]) -> Memory {
    let mut memory = Memory::blank();

    for l in lines {
        memory.debug.insert(memory.code.len(), l.pos.clone());
        let instr = &l.args[0];
        match instr.as_str() {
            "alias" => {
                memory.code.push(Instr::Alias as u64);
                push_str(&mut memory.code, &l.args[1]);
                push_str(&mut memory.code, &l.args[2]);
            },

            "add" => {
                memory.code.push(Instr::Add as u64);
                push_str(&mut memory.code, &l.args[1]);
                push_str(&mut memory.code, &l.args[2]);
            },

            "div" => {
                memory.code.push(Instr::Div as u64);
                push_str(&mut memory.code, &l.args[1]);
                push_str(&mut memory.code, &l.args[2]);
            },

            "input" => {
                memory.code.push(Instr::Input as u64);
                push_str(&mut memory.code, &l.args[1]);
            },

            "jmp" => {
                memory.code.push(Instr::Jmp as u64);
                push_str(&mut memory.code, &l.args[1]);
            },
            "jmp_eq" => {
                memory.code.push(Instr::JmpEq as u64);
                push_str(&mut memory.code, &l.args[1]);
                push_str(&mut memory.code, &l.args[2]);
                push_str(&mut memory.code, &l.args[3]);
                push_str(&mut memory.code, &l.args[4]);
            },
            "jmp_leq" => {
                memory.code.push(Instr::JmpLeq as u64);
                push_str(&mut memory.code, &l.args[1]);
                push_str(&mut memory.code, &l.args[2]);
                push_str(&mut memory.code, &l.args[3]);
                push_str(&mut memory.code, &l.args[4]);
            },

            "mod" => {
                memory.code.push(Instr::Mod as u64);
                push_str(&mut memory.code, &l.args[1]);
                push_str(&mut memory.code, &l.args[2]);
            },

            "mul" => {
                memory.code.push(Instr::Mul as u64);
                push_str(&mut memory.code, &l.args[1]);
                push_str(&mut memory.code, &l.args[2]);
            },

            "pop" => {
                memory.code.push(Instr::Pop as u64);
                push_str(&mut memory.code, &l.args[1]);
            },

            "print" => {
                memory.code.push(Instr::Print as u64);
                memory.code.push(l.args.len() as u64 - 1);
                for s in &l.args[1..] {
                    push_str(&mut memory.code, s);
                }
            },
            "println" => {
                memory.code.push(Instr::Println as u64);
                memory.code.push(l.args.len() as u64 - 1);
                for s in &l.args[1..] {
                    push_str(&mut memory.code, s);
                }
            },
            "print_mem" => {
                memory.code.push(Instr::PrintMem as u64);
            },

            "push" => {
                memory.code.push(Instr::Push as u64);
                push_str(&mut memory.code, &l.args[1]);
            },

            "ret" => {
                memory.code.push(Instr::Ret as u64);
                push_str(&mut memory.code, &l.args[1]);
            },

            "set" => {
                memory.code.push(Instr::Set as u64);
                push_str(&mut memory.code, &l.args[1]);
                push_str(&mut memory.code, &l.args[2]);
            },

            "sqrt" => {
                memory.code.push(Instr::Sqrt as u64);
                push_str(&mut memory.code, &l.args[1]);
            },

            "sub" => {
                memory.code.push(Instr::Sub as u64);
                push_str(&mut memory.code, &l.args[1]);
                push_str(&mut memory.code, &l.args[2]);
            },

            _ => {
                if instr.ends_with(':') {
                    if instr.starts_with('@') {
                        let fname = l.args[0][1..l.args[0].len()-1].to_string();
                        memory.funcs.insert(fname.clone(), Function {
                            mempos: memory.code.len(),
                            args: l.args[1..].to_vec(),
                        });
                    } else {
                        let label = l.args[0][0..l.args[0].len()-1].to_string();
                        memory.labels.entry(label).or_insert(vec![])
                            .push(memory.code.len());
                    }

                    memory.code.push(Instr::Label as u64);
                    memory.code.push(l.args.len() as u64);
                    for a in &l.args {
                        push_str(&mut memory.code, a);
                    }
                } else {
                    memory.code.push(Instr::Call as u64);
                    memory.code.push(l.args.len() as u64);
                    for a in &l.args {
                        push_str(&mut memory.code, a);
                    }
                }
            },
        }
    }

    memory
}
fn execute_bytecode(memory: &mut Memory, mut pos: usize) -> Result<u64, String> {
    let mut is_call = true;
    loop {
        if pos >= memory.code.len() {
            let mut ds: Vec<&String> = memory.debug.values().collect();
            ds.sort();
            let dpos = ds.last()
                .ok_or("reached end of control flow without ret statement (no debug info)".to_string())?;
            return Err(format!("{dpos}:\nreached end of control flow without ret statement"));
        }
        let dpos = &memory.debug[&pos];

        match Instr::try_from(memory.code[pos]) {
            Ok(instr) => {
                match instr {
                    Instr::Alias => {
                        let al1 = pull_str(&memory.code, pos+1);
                        let al2 = pull_str(&memory.code, pos+2+al1.len());

                        if al1 == "-" {
                            // Remove alias
                            memory.aliases.last_mut()
                                .ok_or(format!("{dpos}:\nmissing aliases table"))?
                                .remove(&al2);
                        } else {
                            // Add alias
                            match Alias::get(memory, &al1) {
                                Ok(a) => {
                                    memory.aliases.last_mut()
                                        .ok_or(format!("{dpos}:\nmissing aliases table"))?
                                        .insert(al2.clone(), a);
                                },
                                Err(m) => return Err(format!("{dpos}:\n{m}")),
                            }
                        }

                        pos += 3 + al1.len() + al2.len();
                    },
                    Instr::Add => {
                        let al1 = pull_str(&memory.code, pos+1);
                        let al2 = pull_str(&memory.code, pos+2+al1.len());

                        match Alias::get_and_resolve(memory, &al2) {
                            Ok(v2) => match Alias::get(memory, &al1) {
                                Ok(Alias::Reg(loc1)) => memory.reg[loc1 as usize] += v2,
                                Ok(Alias::Stack(loc1)) => memory.stack[loc1 as usize] += v2,
                                Err(m) => return Err(format!("{dpos}:\n{m}")),
                            },
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }

                        pos += 3 + al1.len() + al2.len();
                    },
                    Instr::Call => {
                        let fname = pull_str(&memory.code, pos+2);
                        match memory.funcs.get(&fname).cloned() {
                            Some(f) => {
                                // Execute function
                                let mut apos = pos+2;
                                let mut args: Vec<String> = vec![];
                                for _ in 0..(memory.code[pos+1]) {
                                    let s = pull_str(&memory.code, apos);
                                    apos += 1 + s.len();
                                    args.push(s);
                                }

                                let mut aliases = HashMap::new();
                                let vals: Result<Vec<u64>, String> = args.iter().skip(1).map(|a| Alias::get_and_resolve(memory, a)).collect();
                                let vals = match vals {
                                    Ok(vs) => vs,
                                    Err(m) => return Err(format!("{dpos}:\n{m}")),
                                };

                                for (argname, argval) in f.args.iter().zip(vals) {
                                    aliases.insert(argname.clone(), Alias::Stack(memory.stack.len() as u64));
                                    memory.stack.push(argval);
                                }
                                memory.aliases.push(aliases);

                                if let Err(m) = execute_bytecode(memory, f.mempos) {
                                    let dpos = &memory.debug[&pos];
                                    return Err(format!("{dpos}:\n{m}"));
                                }

                                memory.aliases.pop();

                                pos = apos;
                            },
                            None => return Err(format!("{dpos}:\ninvalid instruction {fname}")),
                        }
                    },
                    Instr::Div => {
                        let al1 = pull_str(&memory.code, pos+1);
                        let al2 = pull_str(&memory.code, pos+2+al1.len());

                        match Alias::get_and_resolve(memory, &al2) {
                            Ok(v2) => match Alias::get(memory, &al1) {
                                Ok(Alias::Reg(loc1)) => memory.reg[loc1 as usize] /= v2,
                                Ok(Alias::Stack(loc1)) => memory.stack[loc1 as usize] /= v2,
                                Err(m) => return Err(format!("{dpos}:\n{m}")),
                            },
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }

                        pos += 3 + al1.len() + al2.len();
                    },
                    Instr::Input => {
                        let al1 = pull_str(&memory.code, pos+1);

                        let mut input = String::new();
                        io::stdin().read_line(&mut input)
                            .map_err(|m| m.to_string())?;

                        memory.aliases.last_mut()
                            .ok_or(format!("{dpos}:\nmissing aliases table"))?
                            .insert(al1.clone(), Alias::Stack(memory.stack.len() as u64));

                        push_str(&mut memory.stack, &input);

                        pos += 2 + al1.len();
                    },
                    Instr::Jmp => {
                        let label = pull_str(&memory.code, pos+1);

                        pos = next_label(&memory.labels[&label], pos);
                    },
                    Instr::JmpEq => {
                        let al1 = pull_str(&memory.code, pos+1);
                        let al2 = pull_str(&memory.code, pos+2+al1.len());

                        match Alias::get_and_resolve(memory, &al1) {
                            Ok(v1) => match Alias::get_and_resolve(memory, &al2) {
                                Ok(v2) => {
                                    let al3 = pull_str(&memory.code, pos+3+al1.len()+al2.len());
                                    let label = if v1 == v2 {
                                        al3
                                    } else {
                                        pull_str(&memory.code, pos+4+al1.len()+al2.len()+al3.len())
                                    };

                                    pos = next_label(&memory.labels[&label], pos);
                                },
                                Err(m) => return Err(format!("{dpos}:\n{m}")),
                            },
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }
                    },
                    Instr::JmpLeq => {
                        let al1 = pull_str(&memory.code, pos+1);
                        let al2 = pull_str(&memory.code, pos+2+al1.len());

                        match Alias::get_and_resolve(memory, &al1) {
                            Ok(v1) => match Alias::get_and_resolve(memory, &al2) {
                                Ok(v2) => {
                                    let al3 = pull_str(&memory.code, pos+3+al1.len()+al2.len());
                                    let label = if v1 <= v2 {
                                        al3
                                    } else {
                                        pull_str(&memory.code, pos+4+al1.len()+al2.len()+al3.len())
                                    };

                                    pos = next_label(&memory.labels[&label], pos);
                                },
                                Err(m) => return Err(format!("{dpos}:\n{m}")),
                            },
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }
                    },
                    Instr::Label => {
                        let mut lpos = pos+2;
                        for i in 0..(memory.code[pos+1]) {
                            let s = pull_str(&memory.code, lpos);

                            if i == 0 && s.starts_with('@') {
                                if is_call {
                                    is_call = false;
                                } else {
                                    return Err(format!("{dpos}:\ncannot step into function {s}"));
                                }
                            }

                            lpos += 1 + s.len();
                        }

                        pos = lpos;
                    },
                    Instr::Mod => {
                        let al1 = pull_str(&memory.code, pos+1);
                        let al2 = pull_str(&memory.code, pos+2+al1.len());

                        match Alias::get_and_resolve(memory, &al2) {
                            Ok(v2) => match Alias::get(memory, &al1) {
                                Ok(Alias::Reg(loc1)) => memory.reg[loc1 as usize] %= v2,
                                Ok(Alias::Stack(loc1)) => memory.stack[loc1 as usize] %= v2,
                                Err(m) => return Err(format!("{dpos}:\n{m}")),
                            },
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }

                        pos += 3 + al1.len() + al2.len();
                    },
                    Instr::Mul => {
                        let al1 = pull_str(&memory.code, pos+1);
                        let al2 = pull_str(&memory.code, pos+2+al1.len());

                        match Alias::get_and_resolve(memory, &al2) {
                            Ok(v2) => match Alias::get(memory, &al1) {
                                Ok(Alias::Reg(loc1)) => memory.reg[loc1 as usize] *= v2,
                                Ok(Alias::Stack(loc1)) => memory.stack[loc1 as usize] *= v2,
                                Err(m) => return Err(format!("{dpos}:\n{m}")),
                            },
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }

                        pos += 3 + al1.len() + al2.len();
                    },
                    Instr::Pop => {
                        let al1 = pull_str(&memory.code, pos+1);

                        // Safe-guard aliased pops
                        if let Some(aliases) = memory.aliases.last() {
                            for (al, a) in aliases {
                                if let Alias::Stack(loc) = a {
                                    if loc+1 == memory.stack.len() as u64 {
                                        return Err(format!("{dpos}:\ncan't pop {a:?} with alias {al}"));
                                    }
                                }
                            }
                        }

                        match memory.stack.pop() {
                            Some(v2) => match Alias::get(memory, &al1) {
                                Ok(Alias::Reg(loc1)) => memory.reg[loc1 as usize] = v2,
                                Ok(Alias::Stack(_)) => return Err(format!("{dpos}:\ncan't pop into stack")),
                                Err(m) => return Err(m),
                            },
                            None => return Err(format!("{dpos}:\ncan't pop with empty stack")),
                        }

                        pos += 2 + al1.len();
                    },
                    Instr::Print | Instr::Println => {
                        let mut apos = pos+2;
                        for _ in 0..(memory.code[pos+1]) {
                            let a = pull_str(&memory.code, apos);
                            apos += 1 + a.len();

                            if a.starts_with('"') {
                                print!("{}", &a[1..a.len()-1]);
                            } else if let Some(a) = a.strip_prefix('%') {
                                match Alias::get(memory, a) {
                                    Ok(a) => match a.resolve_array(memory) {
                                        Ok(ar) => {
                                            for c in ar {
                                                print!("{}", *c as u8 as char);
                                            }
                                        },
                                        Err(m) => return Err(format!("{dpos}:\n{m}")),
                                    },
                                    Err(m) => return Err(format!("{dpos}:\n{m}")),
                                }
                            } else {
                                match Alias::get_and_resolve(memory, &a) {
                                    Ok(v) => print!("{v}"),
                                    Err(m) => return Err(format!("{dpos}:\n{m}")),
                                }
                            }
                        }

                        if instr == Instr::Println {
                            println!();
                        }

                        pos = apos;
                    },
                    Instr::PrintMem => {
                        eprintln!("{memory:?}");

                        pos += 1;
                    },
                    Instr::Push => {
                        let al1 = pull_str(&memory.code, pos+1);

                        match Alias::get_and_resolve(memory, &al1) {
                            Ok(v1) => memory.stack.push(v1),
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }

                        pos += 2 + al1.len();
                    },
                    Instr::Ret => {
                        let al1 = pull_str(&memory.code, pos+1);

                        match Alias::get_and_resolve(memory, &al1) {
                            Ok(v1) => {
                                memory.stack.push(v1);

                                return Ok(v1);
                            },
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }

                        // unreachable
                    },
                    Instr::Set => {
                        let al1 = pull_str(&memory.code, pos+1);
                        let al2 = pull_str(&memory.code, pos+2+al1.len());

                        match Alias::get_and_resolve(memory, &al2) {
                            Ok(v2) => match Alias::get(memory, &al1) {
                                Ok(Alias::Reg(loc1)) => memory.reg[loc1 as usize] = v2,
                                Ok(Alias::Stack(loc1)) => memory.stack[loc1 as usize] = v2,
                                Err(m) => return Err(format!("{dpos}:\n{m}")),
                            },
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }

                        pos += 3 + al1.len() + al2.len();
                    },
                    Instr::Sqrt => {
                        let al1 = pull_str(&memory.code, pos+1);

                        match Alias::get(memory, &al1) {
                            Ok(Alias::Reg(loc1)) => memory.reg[loc1 as usize] = (memory.reg[loc1 as usize] as f64).sqrt() as u64,
                            Ok(Alias::Stack(loc1)) => memory.stack[loc1 as usize] = (memory.stack[loc1 as usize] as f64).sqrt() as u64,
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }

                        pos += 2 + al1.len();
                    },
                    Instr::Sub => {
                        let al1 = pull_str(&memory.code, pos+1);
                        let al2 = pull_str(&memory.code, pos+2+al1.len());

                        match Alias::get_and_resolve(memory, &al2) {
                            Ok(v2) => match Alias::get(memory, &al1) {
                                Ok(Alias::Reg(loc1)) => memory.reg[loc1 as usize] -= v2,
                                Ok(Alias::Stack(loc1)) => memory.stack[loc1 as usize] -= v2,
                                Err(m) => return Err(format!("{dpos}:\n{m}")),
                            },
                            Err(m) => return Err(format!("{dpos}:\n{m}")),
                        }

                        pos += 3 + al1.len() + al2.len();
                    },
                }
            },
            Err(m) => return Err(format!("{dpos}:\n{m}")),
        }
    }
}

fn main() -> Result<(), String> {
    let filename = std::env::args().nth(1)
        .ok_or("missing filename argument".to_string())?;
    let code = fs::read_to_string(&filename)
        .map_err(|e| format!("{filename}: {e}"))?;
    let lines = split(&filename, &code);
    
    let mut memory = compile_bytecode(&lines);
    let argv = memory.push_argv();
    memory.aliases.last_mut()
        .ok_or("missing aliases table".to_string())?
        .insert("argv".into(), Alias::Stack(memory.stack.len() as u64));
    memory.stack.push(argv[0]);
    let fpos = memory.funcs["main"].mempos;
    match execute_bytecode(&mut memory, fpos) {
        Ok(result) => {
            std::io::stdout().flush()
                .map_err(|e| format!("failed to flush stdout: {e}"))?;
            eprintln!("@main => {result}");
        },
        Err(m) => {
            std::io::stdout().flush()
                .map_err(|e| format!("failed to flush stdout: {e}"))?;

            eprintln!("\n");
            eprintln!("===\n{memory:?}\n");

            let m = m.lines()
                .enumerate()
                .map(|(i, l)| {
                    if i+1 == m.lines().count() {
                        format!("    {l}")
                    } else {
                        l.to_owned()
                    }
                }).collect::<Vec<String>>()
                .join("\n");
            eprintln!("{m}");
        },
    }

    Ok(())
}
